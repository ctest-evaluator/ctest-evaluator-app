# # C-Test Evaluator (Web Application)

This program is part of the C-Test Evaluator, which was developed within the scope of a research project at the University of Duisburg-Essen. The tool can be used to evaluate difficulties for a C-test or to determine the language proficiency level of a user. This application provides the web interface fot interacting with the corresponding server. 

The corresponding repository of the server can be found [here](https://gitlab.com/ctest-evaluator/ctest-evaluator-spring).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
The following items should be installed in your system:
- [Node 12+](https://nodejs.org/en/)
- [NPM 6+](https://nodejs.org/en/)

### Installing
- Clone this repository 
    - ```git clone https://gitlab.com/ctest-evaluator/ctest-evaluator-app.git```
- Install dependencies 
    - ```npm install```
- Rename provided ```.env.example``` to ```.env``` and make any necessary adjustments if the server is reached under a different address.

### Running the web application
- Run the application in development mode with 
    - ```npm run dev```
- Now you are ready to go. Log in with the previously created administrator account and create or import C-Tests over the application interface. 

## Deployment

- Build the application with 
    - ```npm run build```
- Provide the generated application through a web server, e.g. with [Nginx](https://www.nginx.com/).

## Built With

- [nodejs](https://nodejs.org/en/)
- [vuejs](https://vuejs.org/)
- [buefy](https://buefy.org/) 
- [tailwindcss](https://tailwindcss.com/)
