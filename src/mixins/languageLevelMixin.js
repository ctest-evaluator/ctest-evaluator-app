import languages from "@cospired/i18n-iso-languages";
languages.registerLocale(require("@cospired/i18n-iso-languages/langs/de.json"));

const difficultyMap = [
  {
    min: 0,
    max: 1 / 6,
    aggregatedDifficulty: 1,
    speakLevel: "A1",
  },
  {
    min: 1 / 6,
    max: 2 / 6,
    aggregatedDifficulty: 2,
    speakLevel: "A2",
  },
  {
    min: 2 / 6,
    max: 3 / 6,
    aggregatedDifficulty: 3,
    speakLevel: "B1",
  },
  {
    min: 3 / 6,
    max: 4 / 6,
    aggregatedDifficulty: 4,
    speakLevel: "B2",
  },
  {
    min: 4 / 6,
    max: 5 / 6,
    aggregatedDifficulty: 5,
    speakLevel: "C1",
  },
  {
    min: 5 / 6,
    max: 1,
    aggregatedDifficulty: 6,
    speakLevel: "C2",
  },
];

function between(numb, min, max) {
  return numb >= min && numb <= max;
}

export const languageLevelMixin = {
  computed: {
    allLanguages() {
      let languageObj = languages.getNames("de");
      let languageArr = Object.entries(languageObj);
      let sorted = languageArr.sort((a, b) => a[1].localeCompare(b[1]));
      return sorted;
    }
  },
  filters: {
    languageLevelConversion: function(languageLevel) {
      let language = "";
      switch (languageLevel) {
        case 1:
          language = "Anfänger (A1)";
          break;
        case 2:
          language = "Anfänger (A2)";
          break;
        case 3:
          language = "Kompetent (B1)";
          break;
        case 4:
          language = "Kompetent (B2)";
          break;
        case 5:
          language = "Experte (C1)";
          break;
        case 6:
          language = "Experte (C2)";
          break;
      }
      return language;
    },
    getNameByISOCode: function(languageCode) {
      return languages.getName(languageCode.toLowerCase(), "de");
    },
  },
  methods: {
    languageLevelByEstimatedDifficulty(languageLevel) {
      let language = "";
      switch (languageLevel) {
        case 1:
          language = "Anfänger (A1)";
          break;
        case 2:
          language = "Anfänger (A2)";
          break;
        case 3:
          language = "Kompetent (B1)";
          break;
        case 4:
          language = "Kompetent (B2)";
          break;
        case 5:
          language = "Experte (C1)";
          break;
        case 6:
          language = "Experte (C2)";
          break;
      }
      return language;
    },
    languageLevelByEvaluatedDifficulty(languageLevel) {
      return this.languageLevelByEstimatedDifficulty(
        this.diffcultyMapper(languageLevel)
      );
    },
    diffcultyMapper(difficulty) {
      let diffIntervall = difficultyMap.find((intervall) => {
        return between(difficulty, intervall.min, intervall.max);
      });
      return diffIntervall.aggregatedDifficulty;
    },
  },
};
