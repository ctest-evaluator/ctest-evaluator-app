import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";

import "./assets/css/index.css";

import "@mdi/font/css/materialdesignicons.css";
import Buefy from "buefy";
import VueInputAutowidth from "vue-input-autowidth";

Vue.use(Buefy);
Vue.use(VueInputAutowidth);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
