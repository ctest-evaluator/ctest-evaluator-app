import {
  SET_USER_DATA,
  SET_USER_DATA_NATIVE_LANGUAGE,
  SET_USER_DATA_AGE,
  UPDATE_USER_CURRENT_LANGUAGE_LEVEL,
  RESET_USER_DATA,
  ADD_COMPLETED_CTEST,
  ADD_SKIPPED_CTEST,
  RESTORE_SKIPPED_CTESTS,
  ADD_CTEST_TO_PROGRESS,
  SET_CTEST_PROGRESS_STATUS,
} from "../actions/userData";

const getDefaultState = (completedCTests, nativeLanguage, age) => {
  return {
    userData: {
      trainedLanguage: null,
      selfEstimatedLanguageLevel: null,
      currentLanguageLevel: null,
      nativeLanguage: nativeLanguage ? nativeLanguage : null,
      age: age ? age : 1,
    },
    completedCTests: completedCTests ? completedCTests : [],
    skippedCTests: [],
    progress: [],
  };
};

// initial state
const state = getDefaultState();

const getters = {
  getUserData: (state) => state.userData,
  getUserDataNativeLanguage: (state) => state.userData.nativeLanguage,
  getUserDataAge: (state) => state.userData.age,
  isUserDataSet: (state) =>
    state.userData.trainedLanguage && state.userData.selfEstimatedLanguageLevel,
  getCompletedCTests: (state) => state.completedCTests,
  getProgress: (state) => state.progress,
  getSkippedCtests: (state) => state.skippedCTests,
};

const actions = {
  [SET_USER_DATA]: ({ commit }, userData) => {
    commit(SET_USER_DATA, userData);
  },
  [SET_USER_DATA_NATIVE_LANGUAGE]: ({ commit }, nativeLanguage) => {
    commit(SET_USER_DATA_NATIVE_LANGUAGE, nativeLanguage);
  },
  [SET_USER_DATA_AGE]: ({ commit }, age) => {
    commit(SET_USER_DATA_AGE, age);
  },
  [UPDATE_USER_CURRENT_LANGUAGE_LEVEL]: ({ commit }, languageLevel) => {
    commit(UPDATE_USER_CURRENT_LANGUAGE_LEVEL, languageLevel);
  },
  [RESET_USER_DATA]: ({ commit }, resetHard) => {
    commit(RESET_USER_DATA, resetHard);
  },
  [ADD_COMPLETED_CTEST]: ({ commit }, ctestId) => {
    commit(ADD_COMPLETED_CTEST, ctestId);
  },
  [ADD_SKIPPED_CTEST]: ({ commit }, ctestId) => {
    commit(ADD_SKIPPED_CTEST, ctestId);
  },
  [RESTORE_SKIPPED_CTESTS]: ({ commit }) => {
    commit(RESTORE_SKIPPED_CTESTS);
  },
  [ADD_CTEST_TO_PROGRESS]: ({ commit }, ctestProgress) => {
    commit(ADD_CTEST_TO_PROGRESS, ctestProgress);
  },
  [SET_CTEST_PROGRESS_STATUS]: ({ commit }, data) => {
    commit(SET_CTEST_PROGRESS_STATUS, data);
  },
};

const mutations = {
  [RESET_USER_DATA]: (state, resetHard) => {
    resetHard
      ? Object.assign(
          state,
          getDefaultState(
            null,
            state.userData.nativeLanguage,
            state.userData.age
          )
        )
      : Object.assign(
          state,
          getDefaultState(
            state.completedCTests,
            state.userData.nativeLanguage,
            state.userData.age
          )
        );
  },
  [SET_USER_DATA]: (state, data) => {
    state.userData = data;
  },
  [SET_USER_DATA_NATIVE_LANGUAGE]: (state, data) => {
    state.userData.nativeLanguage = data;
  },
  [SET_USER_DATA_AGE]: (state, data) => {
    state.userData.age = data;
  },
  [UPDATE_USER_CURRENT_LANGUAGE_LEVEL]: (state, data) => {
    state.userData.currentLanguageLevel = data;
  },
  [ADD_COMPLETED_CTEST]: (state, data) => {
    if (state.completedCTests.indexOf(data) === -1) {
      state.completedCTests.push(data);
    }
  },
  [ADD_SKIPPED_CTEST]: (state, data) => {
    if (state.skippedCTests.indexOf(data) === -1) {
      state.skippedCTests.push(data);
    }
  },
  [RESTORE_SKIPPED_CTESTS]: (state, data) => {
    state.completedCTests = state.completedCTests.filter( id => {
      return state.skippedCTests.indexOf( id ) < 0;
    });
    state.progress = state.progress.filter( progressItem => {
      return state.skippedCTests.indexOf( progressItem.id ) < 0;
    });
    state.skippedCTests = []
  },
  [ADD_CTEST_TO_PROGRESS]: (state, data) => {
    if (state.progress.filter(p => p.id == data.id).length == 0) {
      state.progress.push(data);
    }
  },
  [SET_CTEST_PROGRESS_STATUS]: (state, data) => {
    state.progress.filter(p => p.id == data.id).map(p => p.status = data.status)
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
