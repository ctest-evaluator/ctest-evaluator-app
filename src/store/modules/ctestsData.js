import {
  SET_CTEST_DATA,
  SET_SELECTED_CTEST_DATA,
  APPEND_CTEST_DATA,
  SET_PROMISE,
  DELETE_CTEST,
  REMOVE_SELECTED_CTEST_DATA,
} from "../actions/ctestsData";
import axios from "@/utils/axios";

const state = { ctests: [], selectedCtest: {}, ctestPromise: null };

const getters = {
  getCtests: (state) => state.ctests,
  getSelectedCtest: (state) => state.selectedCtest,
};

const actions = {
  [SET_CTEST_DATA]: ({ state, commit }) => {
    if (state.ctests && state.ctests.length) {
      return state.ctests;
    }
    if (state.fetchPromise) {
      return state.fetchPromise;
    }
    let promise = axios.get(`/api/ctests`).then((response) => {
      commit("SET_CTEST_DATA", response.data || []);
    });
    commit("SET_PROMISE", promise);
    return promise;
  },
  [APPEND_CTEST_DATA]: ({ commit }, ctest) => {
    commit("APPEND_CTEST_DATA", ctest);
  },
  [SET_SELECTED_CTEST_DATA]: ({ commit }, ctestId) => {
    axios.get(`/api/ctests/` + ctestId).then((response) => {
      let ctest = response.data;
      ctest.nrOfEvaluations = ctest.cTestEvaluations.length;
      commit("SET_SELECTED_CTEST_DATA", ctest);
    });
  },
  [DELETE_CTEST]: async ({ commit }, ctestId) => {
    commit("REMOVE_SELECTED_CTEST_DATA", ctestId)
    const response = await axios.delete(`/api/ctests/` + ctestId);
    if (response.status == 200) {
      commit("REMOVE_SELECTED_CTEST_DATA", ctestId)
    }
    return response;
  },
};

const mutations = {
  [SET_CTEST_DATA]: (state, data) => {
    state.ctests = data;
  },
  [SET_SELECTED_CTEST_DATA]: (state, data) => {
    state.selectedCtest = data;
  },
  [APPEND_CTEST_DATA]: (state, data) => {
    state.ctests.push(data);
  },
  [SET_PROMISE]: (state, data) => {
    state.fetchPromise = data;
  },
  [REMOVE_SELECTED_CTEST_DATA]: (state, ctestId) => {
    state.ctests = state.ctests.filter(c => c.id !== ctestId)
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
