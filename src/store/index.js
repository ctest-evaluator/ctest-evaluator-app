import Vue from "vue";
import Vuex from "vuex";
import user from "./modules/user";
import auth from "./modules/auth";
import userData from "./modules/userData";
import ctestsData from "./modules/ctestsData";

import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  reducer: (state) => ({ userData: state.userData })
})

Vue.use(Vuex);


export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { user, auth, userData, ctestsData },
  plugins: [vuexLocal.plugin]
});
