const apiCall = ({ url, method }) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      try {
        resolve(url, method);
      } catch (err) {
        reject(new Error(err));
      }
    }, 1000);
  });

export default apiCall;
