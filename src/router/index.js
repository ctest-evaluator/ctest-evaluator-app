import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Signin from "../views/Signin.vue";
import Admin from "../views/Admin.vue";
import CTestDetails from "../views/CTestDetails.vue"
import Store from "../store/index";

Vue.use(VueRouter);

const authenticatedC = (to, from, next) => {
  if (Store.getters.isAuthenticated) next();
  else {
    router.push("/")
  }
};

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/signin",
    name: "Signin",
    component: Signin,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
    beforeEnter: authenticatedC,
  },
  {
    path: "/admin/ctest-details/:ctestId",
    name: "CTestDetails",
    component: CTestDetails,
    beforeEnter: authenticatedC,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
